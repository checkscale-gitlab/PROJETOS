import React from 'react'
import Headers from './Headers'
import Articles from './Articles'
import Footer from './Footer'

function App(props){
    return (          
        <div>
            <Headers />
            <Articles />
            <Footer />
        </div>
    );
  };

// const App = () =>
// {
//     const opa = "OLA CAMISINHAS";
//     return <h1>{opa}</h1>
// }

// class App extends React.Component{
//     render()
//     {
//         return(            
//             <div>
//                 <Headers />
//                 <Articles />
//                 <Footer />
//             </div>
//         );
//     }
// }

export default App;